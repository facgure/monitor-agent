# ติดตั้งฝั่ง Client Server

1. `curl -s https://bitbucket.org/facgure/monitor-agent/raw/3f2fca12092da631f2943a3281c05b9887a1b89f/install.sh | sudo sh -s --`


2. ไปตั้งค่า Reverse Proxy

# ตั้งค่า Reverse Proxy
## Client Server

1. สร้างไฟล์ `vim /etc/apache2/conf-enabled/monitoring.conf` โดยใช้คำสั่ง
    ```
    vim monitoring.conf
    ```
2. เพิ่ม script ดังนี้ลงไป
    
    - หากต้องทำเป็น reverse proxy 
      ```
      <Location /prometheus-exporter/>
        Order deny,allow
        Deny from all
        Allow from 206.189.149.213 127.0.0.1
      </Location>
      ProxyPass /prometheus-exporter/node http://172.18.0.1:9100
      ProxyPassReverse /prometheus-exporter/node http://172.18.0.1:9100
      ProxyPass /prometheus-exporter/mysql http://172.18.0.1:9104
      ProxyPassReverse /prometheus-exporter/mysql http://172.18.0.1:9104
      ProxyPass /prometheus-exporter/facgure http://172.18.0.1:9200
      ProxyPassReverse /prometheus-exporter/facgure http://172.18.0.1:9200
      ```
    - กรณีหากต้องทำเป็น Alias
      ```
      <Directory "/var/www/html/files/prometheus-exporter/">
        Order deny,allow
        Deny from all
        Allow from 206.189.149.213 127.0.0.1
      </Directory>

      Alias /prometheus-exporter/node/metrics /var/www/html/files/prometheus-exporter/node-metrics
      Alias /prometheus-exporter/mysql/metrics /var/www/html/files/prometheus-exporter/mysql-metrics
      Alias /prometheus-exporter/facgure /var/www/html/files/prometheus-exporter/facgure
      ```
3. `$ service apache2 reload`

4. หากยังไม่ได้ให้ดูที่ firewall
  ```
  sudo firewall-cmd --list-all
  sudo firewall-cmd --permanent --zone=public --add-port=17000/tcp
  sudo firewall-cmd --permanent --zone=public --add-port=12000/tcp
  sudo firewall-cmd --permanent --zone=public --add-port=9100/tcp
  sudo firewall-cmd --permanent --zone=public --add-port=9104/tcp
  sudo firewall-cmd --permanent --zone=public --add-port=9200/tcp
  sudo firewall-cmd --reload
  ```