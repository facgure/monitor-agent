#!/bin/bash
if [ `id -u` -ne 0 ]; then echo "Please run as root" ; exit 1 ; fi

if [ -x "$(command -v apt-get)" ]; then apt-get -y install unzip jq wget; fi

if [ -x "$(command -v yum)" ]; then sudo yum -y install unzip wget; fi

wget https://bitbucket.org/facgure/monitor-agent/raw/1fdb6a3ca770df819116469c6a755b9d09a61917/node_exporter.zip -O /tmp/node_exporter.zip

if [ ! -e /tmp/node_exporter.zip ] ; then echo "Fail to download node_exporter.zip" ; exit 1 ; fi

cd /tmp/

unzip node_exporter.zip

chmod 0777 node_exporter

sudo mv node_exporter /usr/local/bin/

if [ ! -e /usr/local/bin/node_exporter ] ; then echo "Fail to move node_exporter" ; exit 1 ; fi

echo "
[Unit]
Description=Node Exporter
After=network.target
[Service]
Type=simple
ExecStart=/usr/local/bin/node_exporter
[Install]
WantedBy=multi-user.target" > /etc/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl restart node_exporter
sudo systemctl status node_exporter
sudo systemctl enable node_exporter